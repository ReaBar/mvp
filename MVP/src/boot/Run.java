package boot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Observable;

import gui.MazeWindow;
import model.Model;
import model.MyModel;
import presenter.Presenter;
import view.MyViewCLI;
import view.View;

/**
 * The Class Run.
 * Contains the main method
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class Run {
//
//	/**
//	 * This is the main method which runs the program.
//	 * 
//	 * @param args the arguments
//	 */
	public static void main(String[] args) throws IOException{
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		PrintWriter writer = new PrintWriter(System.out, true);
		View view = null;
		//Properties p = new Properties();
		//p = p.Start();
		//Model model = new MyModel( p.getNumOfThreads());
		Model model = new MyModel();
		view = new MazeWindow("this is our project", 500, 300);
		 Presenter presenter = new Presenter(view, model);
		 ((Observable) model).addObserver(presenter);
		 ((Observable) view).addObserver(presenter);
		 
		 view.start();
		 if(view.whatKindOfView().equals("CLI")){
			 view = new MyViewCLI(writer, reader);
			Presenter presenter2 = new Presenter(view, model);
			 ((Observable) view).addObserver(presenter2);
			 ((Observable) model).deleteObserver(presenter);
			 ((Observable) model).addObserver(presenter2);
			 view.start();
		 }
		 	/*if(p.getView().equals("cli")){
			 view = new MyViewCLI(writer, reader);
			 Presenter presenter = new Presenter(view, model);
			 ((Observable) model).addObserver(presenter);
			 ((Observable) view).addObserver(presenter);
			 
			 view.start();
		}
		else if(p.getView().equals("gui")){
			
			view = new MazeWindow("this is our project", 500, 300);
			 Presenter presenter = new Presenter(view, model);
			 ((Observable) model).addObserver(presenter);
			 ((Observable) view).addObserver(presenter);
			 
			
			 view.start();
		}
		
*/
		//Thread t = new Thread(new MyController(view, model));
		//t.start();
	}

}
