package presenter;

import java.io.File;
import java.io.Serializable;
import java.util.Scanner;

import myXMLEncodeDecode.myXMLEncodeDecode;

/**
 * The Class Properties.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class Properties implements Serializable {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2315970338694591223L;
	
	/** The num of threads. */
	private int numOfThreads;
	
	/** The algorithm to solve. */
	private String algorithmToSolve;
	
	/** The algorithm to generate. */
	private String algorithmToGenerate;
	
	/** The view. */
	private String view;
	
	/** The file path. */
	String filePath = new String("./Properties/properties.xml");

	/**
	 * Instantiates a new properties.
	 */
	public Properties(){
	}
	
	/**
	 * Instantiates a new properties.
	 *
	 * @param view the view
	 * @param algoToSolve the algorithm to solve
	 * @param numOfThreads the number of threads
	 */
	public Properties(String view,String algoToSolve, int numOfThreads){
		this.deletePropeties();
		this.view = view;
		this.algorithmToSolve = algoToSolve;
		this.numOfThreads = numOfThreads;
		myXMLEncodeDecode xml = new myXMLEncodeDecode();
		new File("./Properties").mkdir();
		xml.writeToXml(filePath, this);
	}
	
	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public String getView() {
		return view;
	}

	/**
	 * Sets the view.
	 *
	 * @param view the new view
	 */
	public void setView(String view) {
		this.view = view;
	}

	/**
	 * Gets the algorithm to solve.
	 *
	 * @return the algorithm to solve
	 */
	public String getAlgorithmToSolve() {
		return algorithmToSolve;
	}

	/**
	 * Sets the algorithm to solve.
	 *
	 * @param algorithmToSolve the new algorithm to solve
	 */
	public void setAlgorithmToSolve(String algorithmToSolve) {
		this.algorithmToSolve = algorithmToSolve;
	}

	/**
	 * Gets the algorithm to generate.
	 *
	 * @return the algorithm to generate
	 */
	public String getAlgorithmToGenerate() {
		return algorithmToGenerate;
	}

	/**
	 * Sets the algorithm to generate.
	 *
	 * @param algorithmToGenerate the new algorithm to generate
	 */
	public void setAlgorithmToGenerate(String algorithmToGenerate) {
		this.algorithmToGenerate = algorithmToGenerate;
	}

	/**
	 * Gets the serialversionuid.
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * Gets the num of threads.
	 *
	 * @return the num of threads
	 */
	public int getNumOfThreads() {
		return numOfThreads;
	}

	/**
	 * Sets the num of threads.
	 *
	 * @param numOfThreads the new num of threads
	 */
	public void setNumOfThreads(int numOfThreads) {
		this.numOfThreads = numOfThreads;
	}

	/**
	 * Start.
	 * get the properties from the xml file or get a new properties if there is no file
	 *
	 * @return the properties
	 */
	public Properties Start(){
		Properties p = null;
		int num;
		myXMLEncodeDecode xml = new myXMLEncodeDecode();
		File f = new File(filePath);
		if(f.exists()){
			p = (Properties) xml.readFromXml(filePath);
		}
		else{
			Scanner in = new Scanner(System.in);
			p = new Properties();
			do {
				System.out.println("How many threads to run in the thread pool?");
				p.setNumOfThreads(in.nextInt());
			} while (p.getNumOfThreads() <= 0);

			/*do {
				System.out.println("With which algorithm to generate the maze:\n1.DFS\n2.Randomly");
				num = in.nextInt();
			} while (num > 2 || num < 1 );
			switch (num) {
			case 1:
				p.setAlgorithmToGenerate("MyMaze3dGenerator");
				break;
			case 2:
				p.setAlgorithmToGenerate("SimpleMaze3dGenerator");
				break;
			}*/

			do {
				System.out.println("With which algorithm to solve the maze:\n1.A* - Manhattan Distance\n2.A* - Air Distance\n3.BFS");
				num = in.nextInt();
			} while (num > 3 || num < 1);

			switch (num) {
			case 1:
				p.setAlgorithmToSolve("AStarManhattan");
				break;
			case 2:
				p.setAlgorithmToSolve("AStarAir");
				break;
			case 3:
				p.setAlgorithmToSolve("BFS");
				break;
			}


			do {
				System.out.println("Which view would you like to use?\n1.Command line\n2.Graphic User Interface");
				num = in.nextInt();
			} while (num > 2 || num < 1 );
			switch (num) {
			case 1:
				p.setView("CLI");
				break;
			case 2:
				p.setView("GUI");
				break;
			}

			new File("./Properties").mkdir();
			xml.writeToXml(filePath, p);
		}
		
		return p;
	}

	/**
	 * Sets the properties.
	 *
	 * @param view the view
	 * @param algorithm the algorithm
	 * @param numOfThreads the num of threads
	 * @return the properties
	 */
	public Properties setProperties(String view, String algorithm, int numOfThreads){
		Properties p = new Properties();
		p.setView(view);
		p.setAlgorithmToSolve(algorithm);
		p.setNumOfThreads(numOfThreads);
		return p;
	}
	
	/**
	 * Delete propeties.
	 */
	public void deletePropeties(){
		File file = new File("./Properties/properties.xml");
		if(file.exists()){
			file.delete();
		}
	}
}


