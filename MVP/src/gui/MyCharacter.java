package gui;


import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

import algorithms.mazeGenerators.Position;

/**
 * The Class MyCharacter.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class MyCharacter {

	/** The current position. */
	private Position currentPosition;

	/**
	 * Gets the current position.
	 *
	 * @return the current position
	 */
	public Position getCurrentPosition() {
		return currentPosition;
	}

	/**
	 * Sets the current position.
	 *
	 * @param currentPosition the new current position
	 */
	public void setCurrentPosition(Position currentPosition) {
		this.currentPosition = currentPosition;
	}

//	/**
//	 * Paint character.
//	 *
//	 * @param dpoints the dpoints
//	 * @param cheight the cheight
//	 * @param w0 the w0
//	 * @param w1 the w1
//	 * @param h the h
//	 * @param e the e
//	 */
//	public void paintCharacter(double[] dpoints, double cheight, double w0, double w1, double h, PaintEvent e) {
//
//		e.gc.setBackground(new Color(null,200,0,0));
//		e.gc.fillOval((int)Math.round(dpoints[0]), (int)Math.round(dpoints[1]-cheight/2), (int)Math.round((w0+w1)/2), (int)Math.round(h));
//		e.gc.setBackground(new Color(null,255,0,0));
//		e.gc.fillOval((int)Math.round(dpoints[0]+2), (int)Math.round(dpoints[1]-cheight/2+2), (int)Math.round((w0+w1)/2/1.5), (int)Math.round(h/1.5));
//		e.gc.setBackground(new Color(null,0,0,0));		
//	}

	/**
	 * Paint.
	 *
	 * @param dpoints the dpoints
	 * @param cheight the cheight
	 * @param w0 the w0
	 * @param w1 the w1
	 * @param h the h
	 * @param e the e
	 */
	public void paint(double[] dpoints, double cheight, double w0, double w1, double h, PaintEvent e){
		Image image = new Image(e.display, "./resources/emoji.png");
		e.gc.setBackground(new Color(null,200,0,0));
		e.gc.drawImage(image, 0, 0, image.getBounds().width,image.getBounds().height,(int)Math.round(dpoints[0]), (int)Math.round(dpoints[1]-cheight/2), (int)Math.round((w0+w1)/2), (int)Math.round(h));
		e.gc.setBackground(new Color(null,255,0,0));
		e.gc.setBackground(new Color(null,0,0,0));		
	
	
	}

}
