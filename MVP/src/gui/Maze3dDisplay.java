package gui;

import java.util.ArrayList;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

import algorithms.mazeGenerators.Maze3d;
import algorithms.mazeGenerators.Position;

/**
 * The Class Maze3dDisplay.
 * 
 * @author Rea Bar and Tom Eileen Hirsch
 */
public class Maze3dDisplay extends MazeDisplay{

	/** The maze data. */
	private int[][][] mazeData;
	
	/** The start position. */
	private Position startPosition;
	
	/** The maze cross. */
	private int[][] mazeCross;
	
	/** The goal position. */
	private Position goalPosition;

	/**
	 * Gets the maze data.
	 *
	 * @return the maze data
	 */
	public int[][][] getMazeData() {
		return mazeData;
	}

	/**
	 * Sets the maze data.
	 *
	 * @param mazeData the new maze data
	 */
	public void setMazeData(int[][][] mazeData) {
		this.mazeData = mazeData;
	}

	/**
	 * Gets the start position.
	 *
	 * @return the start position
	 */
	public Position getStartPosition() {
		return startPosition;
	}

	/**
	 * Sets the start position.
	 *
	 * @param startPosition the new start position
	 */
	public void setStartPosition(Position startPosition) {
		this.startPosition = startPosition;
	}

	/**
	 * Gets the maze cross.
	 *
	 * @return the maze cross
	 */
	public int[][] getMazeCross() {
		return mazeCross;
	}

	/**
	 * Sets the maze cross.
	 *
	 * @param mazeCross the new maze cross
	 */
	public void setMazeCross(int[][] mazeCross) {
		this.mazeCross = mazeCross;
	}

	/**
	 * Gets the goal position.
	 *
	 * @return the goal position
	 */
	public Position getGoalPosition() {
		return goalPosition;
	}

	/**
	 * Sets the goal position.
	 *
	 * @param goalPosition the new goal position
	 */
	public void setGoalPosition(Position goalPosition) {
		this.goalPosition = goalPosition;
	}

	/**
	 * Instantiates a new maze3d display.
	 *
	 * @param arg0 the Composite
	 * @param arg1 the int
	 */
	public Maze3dDisplay(Composite arg0, int arg1) {
		super(arg0, arg1);

		final Color white=new Color(null, 255, 255, 255);
		//final Color black=new Color(null, 150,150,150);
		setBackground(white);
		addPaintListener(new PaintListener() {

			@Override
			public void paintControl(PaintEvent e) {
				e.gc.setForeground(new Color(null,0,0,0));
				e.gc.setBackground(new Color(null,0,0,0));

				int width=getSize().x;
				int height=getSize().y;
				

				if(mazeCross != null){
					

					//					int mx=width/2;
					//					
					//					int w=width/mazeCross[0].length;
					//					int h=height/mazeCross.length;
					//
					//					for(int i=0;i<mazeCross.length;i++){
					//						double w0=0.5*w +0.5*w*i/mazeCross.length;
					//						double w1=0.5*w +0.5*w*(i+1)/mazeCross.length;
					//						double start=mx-w0*mazeCross[i].length/2;
					//						double start1=mx-w1*mazeCross[i].length/2;
					//					
					//						for(int j=0;j<mazeCross[i].length;j++){
					//							
					//							double []dpoints={start+j*w0,i*h,start+j*w0+w0,i*h,start1+j*w1+w1,i*h+h,start1+j*w1,i*h+h};
					//							double cheight=h/2;
					//							int x=j*w;
					//							int y=i*h;
					//							if(mazeCross[i][j]!=0)
					//								e.gc.fillRectangle(x,y,w,h);
					//							if(i==character.getCurrentPosition().getX() && j==character.getCurrentPosition().getZ()){
					//								 character.paintCharacter(dpoints,cheight,w0,w1,h,e);	  
					//							}
					//						}}

					int mx=width/2;

					double w=(double)width/mazeCross[0].length;
					double h=(double)height/mazeCross.length;

					for(int i=0;i<mazeCross.length;i++){
						double w0=0.5*w +0.5*w*i/mazeCross.length;
						double w1=0.5*w +0.5*w*(i+1)/mazeCross.length;
						double start=mx-w0*mazeCross[i].length/2;
						double start1=mx-w1*mazeCross[i].length/2;
						for(int j=0;j<mazeCross[i].length;j++){
							double []dpoints={start+j*w0,i*h,start+j*w0+w0,i*h,start1+j*w1+w1,i*h+h,start1+j*w1,i*h+h};
							double cheight=h/2;
							if(mazeCross[i][j]==1)
								paintCube(dpoints, cheight,e);
							if(mazeCross[i][j]==2)
								paintGoal(dpoints,cheight,w0,w1,h,e);
							if(i==character.getCurrentPosition().getX() && j==character.getCurrentPosition().getZ()){
								// character.paintCharacter(dpoints,cheight,w0,w1,h,e);
								character.paint(dpoints,cheight,w0,w1,h,e);		
							}
							if(i==character.getCurrentPosition().getX() && j==character.getCurrentPosition().getZ() && mazeCross[i][j]==2){
								character.paint(dpoints,cheight,w0,w1,h,e);	
					
								popUpImg();
			
							}
							

						}
					}

				}
			}});
	}


	/**
	 * Pop up image for solving the maze.
	 */
	protected void popUpImg() {
		final Shell dialog = new Shell(getShell());

		dialog.setText("Congratulations!");
		//dialog.setVisible(false);
		
		Image im = new Image(dialog.getDisplay(), "./resources/goal.jpg");
		
		dialog.addListener(SWT.Paint, new Listener() {
			
			@Override
			public void handleEvent(Event e) {
				
				dialog.setSize(im.getBounds().width/4, im.getBounds().height/4);
				
				e.gc.drawImage(im, 0, 0, im.getBounds().width, im.getBounds().height, 0, 0, im.getBounds().width/4, im.getBounds().height/4);
				
			}
		});
		
		
		dialog.pack();
		//if(!dialog.isVisible()){
		dialog.open();
			//dialog.setVisible(true);
		//}
		//dialog.forceFocus();
		
	}

	/**
	 * Paint goal image.
	 *
	 * @param dpoints the dpoints
	 * @param cheight the cheight
	 * @param w0 the w0
	 * @param w1 the w1
	 * @param h the h
	 * @param e the e
	 */
	protected void paintGoal(double[] dpoints, double cheight, double w0, double w1, double h, PaintEvent e) {
	
		Image image = new Image(e.display, "./resources/finish.png");
		e.gc.setBackground(new Color(null,200,0,0));
		e.gc.drawImage(image, 0, 0, image.getBounds().width,image.getBounds().height,(int)Math.round(dpoints[0]), (int)Math.round(dpoints[1]-cheight/2), (int)Math.round((w0+w1)/2), (int)Math.round(h));
		e.gc.setBackground(new Color(null,255,0,0));
		e.gc.setBackground(new Color(null,0,0,0));	

	}

	/**
	 * Paint cube.
	 *
	 * @param p the p
	 * @param h the h
	 * @param e the e
	 */
	private void paintCube(double[] p,double h,PaintEvent e){
		int[] f=new int[p.length];
		for(int k=0;k<f.length;f[k]=(int)Math.round(p[k]),k++);

		e.gc.drawPolygon(f);

		int[] r=f.clone();
		for(int k=1;k<r.length;r[k]=f[k]-(int)(h),k+=2);

		int[] b={r[0],r[1],r[2],r[3],f[2],f[3],f[0],f[1]};
		e.gc.drawPolygon(b);
		int[] fr={r[6],r[7],r[4],r[5],f[4],f[5],f[6],f[7]};
		e.gc.drawPolygon(fr);

		e.gc.fillPolygon(r);
	}

	/* (non-Javadoc)
	 * @see gui.MazeDisplay#printMaze(byte[])
	 */
	@Override
	public void printMaze(byte[] mazeByteArray) {

		Maze3d maze = new Maze3d(mazeByteArray);
		setMazeData(maze.getMaze());
		setStartPosition(maze.getStartPosition());
		setGoalPosition(maze.getGoalPosition());
		character = new MyCharacter();
		character.setCurrentPosition(this.getStartPosition());

		//setMazeCross(maze.getCrossSectionByY(getStartPosition().getY()));
		DisplayCross(maze.getCrossSectionByY(getStartPosition().getY()));
		redraw();


	}


	/* (non-Javadoc)
	 * @see gui.MazeDisplay#DisplayCross(int[][])
	 */
	@Override
	public void DisplayCross(int[][] crossSectionBy) {

		if(character.getCurrentPosition().getY()==getGoalPosition().getY()){
			int x = getGoalPosition().getX();
			int z = getGoalPosition().getZ();
			int t = crossSectionBy.length;
			int p = crossSectionBy[0].length;
			int[][] temp = new int[t][p];
			for(int tempT = 0;tempT<t;tempT++)
			{
				for(int tempP = 0;tempP<p;tempP++)
				{
					if(tempT == x && tempP==z)
						temp[tempT][tempP] = 2;
					else
						temp[tempT][tempP] = crossSectionBy[tempT][tempP];
				}
			}

			setMazeCross(temp);

		}
		else
			setMazeCross(crossSectionBy);

		redraw();
	}



	/* (non-Javadoc)
	 * @see gui.MazeDisplay#moveCaracter(java.lang.Object)
	 */
	@Override
	public void moveCaracter(Object arg) {
		character.setCurrentPosition((Position) arg);
		redraw();

	}

	/* (non-Javadoc)
	 * @see gui.MazeDisplay#displaySolution(java.lang.Object)
	 */
	@Override
	public void displaySolution(Object solution) {
		ArrayList<Position> tempsolution = new ArrayList<>();
		ArrayList<Position> hint = new ArrayList<>();
		//String sol = solution.toString();
		String[] tempString = solution.toString().replace("[", "").replace("{", "").replace("}", "").replace(" ", "").replace("]", "").split(",");
		int[] tempIntArray = new int[tempString.length];
		for(int i=0;i<tempString.length;i++){
			tempIntArray[i] = Integer.parseInt(tempString[i]);
		}
		for(int j=0;j<tempIntArray.length-2;j+=3){
			tempsolution.add(new Position(tempIntArray[j], tempIntArray[j+1], tempIntArray[j+2]));
		}
		

		for(int i=0;i<tempsolution.size();i++){
			if(character.getCurrentPosition().equals(tempsolution.get(i))){
				hint.add(tempsolution.get(i));
				for(int j=i+1;j<i+6;j++)
					if(j<tempsolution.size())
						hint.add(tempsolution.get(j));
			}
		}
		if(hint.isEmpty()){
			MessageBox popMsg = new MessageBox(getShell(), SWT.OK | SWT.ICON_INFORMATION);
			popMsg.setText("hint");
			popMsg.setMessage("sorry, there is no hint for your position");
			popMsg.open();
		}
		else{
			ArrayList<String> out = new ArrayList<>();
			for(int i=0; i<hint.size()-1;i++){
				Position p = hint.get(i);
				Position next = hint.get(i+1);
				if(p.getX()!=next.getX())
					if(p.getX()>next.getX())
						out.add("forward");
					else
						out.add("backward");
				if(p.getY()!=next.getY())
					if(p.getY()>next.getY())
						out.add("down");
					else
						out.add("up");
				if(p.getZ()!=next.getZ())
					if(p.getZ()>next.getZ())
						out.add("left");
					else
						out.add("right");
					
			}
			
			MessageBox popMsg = new MessageBox(getShell(), SWT.OK | SWT.ICON_INFORMATION);
			popMsg.setText("hint");
			popMsg.setMessage(out.toString());
			popMsg.open();
		}
		

	}

}
